from pylab import *

n = arange(-10,10,0.001);
f1 = (0.9**n)*cos(pi/6*n-pi/3);
plot(n,f1,label="freq = pi/6");

f2 = (1.1**n)*cos(49*pi/6*n-pi/3);
plot(n,f2,label="freq = 49pi/6");
legend();
show();
