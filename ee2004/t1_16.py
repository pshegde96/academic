
from pylab import *
import cmath

def funct(s,n):
	return exp(s*n);

n = arange(-5,6,1);
s = -0.1;
f_real = funct(s,n);

t = arange(-5,6,0.01);
f_cont = funct(s,t);

s = complex(-0.1,2*cmath.pi)
f_complex = funct(s,n).real;


plot(n,f_real,'ro');
plot(n,f_complex,'g^');
plot(t,f_cont);
show();
