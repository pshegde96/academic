'''
**the function is x(t)=cost(2*pi*f0*t)
* We sample this function at different Ts and check the accuracy
'''
from pylab import *

def funct(T):
	f0 = 50;
	n = arange(0,40*(10**-3)+T,T);
	F = cos(2*pi*f0*n);
	return (F,n);

#T = 125us
T = 125*(10**-6);
f1 = funct(T);
figure();
title("T = 125us");
plot(f1[1],f1[0]);

#T = 1ms
T = 10**-3;
f2 = funct(T);
figure();
title("T = 1ms");
plot(f2[1],f2[0]);
plot(f1[1],f1[0]);

#T = 2ms
T = 2*(10**-3);
f3 = funct(T);
figure();
title("T = 2ms");
plot(f3[1],f3[0]);
plot(f1[1],f1[0]);

#T = 10ms
T = 10**-2;
f4 = funct(T);
figure();
title("T = 10ms");
plot(f4[1],f4[0]);
plot(f1[1],f1[0]);

show();
