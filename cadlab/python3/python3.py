from pylab import *
from scipy import *
from scipy.linalg import lstsq
import scipy.special as sp


A = 1.05;
B = -0.105;
sigma = logspace(-1,-3,9);

def gfunct(t,A,B):
	return (A*sp.jn(2,t)+B*t);

data = loadtxt("fitting.dat");

plot(data[:,0],data[:,1:]);

t = data[:,0];
x0 = gfunct(t,A,B);
plot(t,x0);
show();

data1 = data[:,1];
errorbar(t[::5],data1[::5],sigma[0],fmt ='ro');
plot(t,x0);
show();

M0 = sp.jn(2,t);
M1 = t;
M = c_[M0,M1];
AB0 = array([A,B]).reshape((2,1));
print AB0;

g = dot(M,AB0);
x0 = x0.reshape((101,1));
print array_equal(g,x0);

a = linspace(0.0,2.0,21);
b = linspace(-0.2,0.0,21);

error = array([[[0 for i in range(0,10)] for i in range(0,21)]for i in range(0,21)],dtype = float64);
p = array([[0 for i in range(0,2)]for i in range(0,10)],dtype = float64);
for k in range(1,10):
	for i in range(0,21):
		for j in range(0,21):
			error[i,j,k] = sum(pow((data[:,k] - gfunct(t,a[i],b[j])),2));
	err,resid,rank,sig=lstsq(M,data[:,k]);
	p[k,:] = array(err,dtype = float64);
	#aa,bb = meshgrid(a,b);
	#contour(a,b,error[:,:,k]);
	#show();	
aa,bb = meshgrid(a,b);
contour(a,b,error[:,:,1]);
show();

axis([0.001,0.1,0.0,1.2]);
plot(sigma,p[1:10,0]);
show();

axis([0.001,0.1,0.0,1.2]);
loglog(sigma,p[1:10,0]);
show();

axis([0.001,0.1,0.0,1.2]);
loglog(sigma,abs(p[1:10,1]));
show();

