/***************************************************************************************
ASSIGNMENT 1.1
Author: Parikshit S Hegde(EE14B123)
This Program accepts a paragraph from a text file, and gives the statistics of the number
of letters in the word.
***********************************************************************************/



#include<stdio.h>

#include<stdlib.h>

#define MAXLENGTH 512

void graph_plotter(int*);

int main(int argc, char **argv)

{

	int letter_counter=0,word_counter[11],i;

	for(i=0;i<=10;i++)

		word_counter[i] = 0;	/*Setting all word counts to 0 initially*/

	

	/* Program expects a filename. Check that argument was passed */

	if(argc != 2){

		printf("Usage ./a.out <filename>");

		exit(1);

	}

	/* Open file while checking for existence */

	FILE *fp = fopen(argv[1], "r");

	if(fp == NULL){

		printf("File could not be opened");

		exit(2);

	}

	/* Read in lines from file and process.*/

	

	char buff[MAXLENGTH];

	/* also allocate variables to hold counting information */

	while(fgets(buff, MAXLENGTH, fp)){	/*Fetches 1 line at a time and stores it in buff*/

		i=0;

		while(1)

		{

			if(((buff[i]>=65)&&(buff[i]<=90))||((buff[i]>=97)&&(buff[i]<=122))) /*Checking for an alphabet*/

				letter_counter++;

			switch (buff[i])	/*Looking for word-separators*/

			{

				case ' ':

				case ',':

				case '.':

				case '\t':

					word_counter[letter_counter]++;

					letter_counter = 0;

					break;

			}

			if(buff[i] == '\0')	/*If end of string, increment the last word and fetch next sentence*/

			{

				word_counter[letter_counter]++;

				letter_counter=0;

				break;

			}

			i++;

		}

		

	

		

	}

	for(i=3;i<=10;i++)		/*Printing out the data in word form*/

			printf("Number of words with %d letters is: %d \n",i,word_counter[i]);

	

/*graph_plotter functions prints out the word count data in graph form*/

	graph_plotter(word_counter);	

	}

	void graph_plotter(int var[11])

	{

		int i,j,max=0;

		for(i=1;i<=10;i++)	/*Find the maximum value in the array so as to space the graph properly*/

			if(var[i]>max)

				max = i;

		for(i=max;i>=1;i--)		/*Nested loops to print the graph*/

		{

			printf("| \t");

			for(j=1;j<=10;j++){

				if(var[j] >= i)

					printf("* \t");

				else

					printf("  \t");

			

			}

			printf("\n");

		}

		printf("+------------------------------------------------------------------------------------- \n");

		for(i=0;i<=10;i++)

			printf("%d \t",i);

		printf("\n");

		return;

		

	}
