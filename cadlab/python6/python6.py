from pylab import *
import scipy.signal as signal
from scipy import *
t = linspace(0,20,201);

#Step response of the spring system
num1 = poly1d([0.1,0,0]);
den1 = poly1d([1,0,1]);
sys2 = signal.lti(num1,den1);
y1 = sys2.step(T=t)[1];
figure();
subplot(2,1,1);
plot(t,y1,'b');
title(r"Displacement in 1D spring system using impulse response");
xlabel(r"time$\rightarrow$");
ylabel(r"Displacement$\rightarrow$");

#Impulse response of the spring system
num2 = poly1d([0.1,0]);
den2 = poly1d([1,0,1]);
sys1 = signal.lti(num2,den2);
y2 = sys1.impulse(T=t)[1];
subplot(2,1,2);
plot(t,y2,'r');
title(r"Displacement in 1D spring system using step response");
xlabel(r"time$\rightarrow$");
ylabel(r"Displacement$\rightarrow$");

#2-d spring system
#Solving for x
num3 = poly1d([1,0,2]);
den3 = poly1d([1,0,3,0]);
sys3 = signal.lti(num3,den3);
x3 = sys3.impulse(T=t)[1];
figure();
subplot(2,1,1);
title(r"X Displacement in a 2D spring system");
xlabel(r"time$\rightarrow$");
ylabel(r"x$\rightarrow$")
plot(t,x3);

#Solving for y
num4 = poly1d([2,0]);
den4 = poly1d([1,0,3,0,0]);
sys4 = signal.lti(num4,den4);
y4 = sys4.impulse(T=t)[1];
subplot(2,1,2);
title(r"Y Displacement in a 2D spring system");
xlabel(r"time$\rightarrow$");
ylabel(r"y$\rightarrow$");
plot(t,y4);


#Response of the given RL circuit.
R1 =10 
R2 =100
L = 0.001
omega = logspace(3,9,61).reshape(61,1);#omega vector
H = R2/((R1+R2)+1j*omega*L); #The transfer function
#Plot w.r.t log(omega)
#Magnitude plot of Transfer function
figure();
subplot(2,1,1);
title(r"Magnitude Plot")
xlabel(r"$\Omega \rightarrow$");
ylabel(r"$\mid$H$\mid \rightarrow$")
loglog(omega,abs(H),'ro');
#Phase plot 
subplot(2,1,2);
title(r"Phase Plot")
xlabel(r"$\Omega \rightarrow$");
ylabel(r"$\angle$H $\rightarrow$");
semilogx(omega,180*angle(H)/pi,'ro');

#Plot w.r.t frequency
frequency = omega/2/pi;
figure();
#Magnitude plot
subplot(2,1,1);
title(r"Magnitude Plot")
xlabel(r"f $\rightarrow$");
ylabel(r"$\mid$H$\mid \rightarrow$")
loglog(frequency,abs(H),'ro');
#Phase plot
subplot(2,1,2);
title(r"Phase Plot")
xlabel(r"f $\rightarrow$");
ylabel(r"$\angle$H $\rightarrow$");
semilogx(frequency,180*angle(H)/pi,'ro');

#Solving the system for given input
t1 = linspace(0,0.01,pow(10,5)+1);
#t1 = linspace(0,3*pow(10,-5),6001); #To examine transient response at the start
#t1 = linspace(0.005,0.005+3*pow(10,-5),6001);#To examine steady state response in a small interval
num5 = poly1d([100*(pow(10,12)-pow(10,6)),0]);
den5 = polymul(poly1d([1,0,pow(10,6)+pow(10,12),0,pow(10,18)]),poly1d([0.001,110]));
sys5 = signal.lti(num5,den5);
y5 = sys5.impulse(T=t1)[1];
figure();
title(r"Time Response of the system to given input")
xlabel(r"time$\rightarrow$");
ylabel(r"Output voltage$\rightarrow$");
plot(t1,y5);
show();
