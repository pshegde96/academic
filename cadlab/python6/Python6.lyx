#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\begin_modules
noweb
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
PYTHON 6: LAPLACE TRANSFORM
\end_layout

\begin_layout Author
PARIKSHIT S HEGDE(EE14B123)
\end_layout

\begin_layout Date
08-11-2015
\end_layout

\begin_layout Abstract
In this program, we attempt to demonstrate the use of Laplace Transform
 in solving differential equations.
 First, the 1D and 2D spring problems are solved by finding out the differential
 equation describing the motion and then use Laplace Transform to solve.
 Then, the transfer function of a given R-L circuit is found and its behaviour
 is studied.
 Then, an input is applied and using the multiplicative property of Laplace
 Transform, the output voltage is obtained.
 Graphs are plotted at all instances to study the behaviour.
\end_layout

\begin_layout Part*
THE PROGRAM:
\end_layout

\begin_layout Subsection*
SOLVING FOR 1D SPRING SYSTEM:
\end_layout

\begin_layout Standard
The spring system is given by this characteristic equation in the time domain:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ddot{x}+x=0$
\end_inset


\end_layout

\begin_layout Standard
We solve the above differential equation using the Laplace Transform.
 The equation in the Laplace domain is given by:
\end_layout

\begin_layout Standard
\begin_inset Formula $X(s)=\frac{s\times x(0)+\dot{x}(0)}{s^{2}+1}$
\end_inset


\end_layout

\begin_layout Standard
We get the solution to the equation by finding out the impulse response
 to X(s), or the step response to sX(s).We have implemented both the methods
 in the program.
\end_layout

\begin_layout LyX-Code
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
*
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

from pylab import *
\end_layout

\begin_layout Plain Layout

import scipy.signal as signal
\end_layout

\begin_layout Plain Layout

from scipy import *
\end_layout

\begin_layout Plain Layout

t = linspace(0,20,201);
\end_layout

\begin_layout Plain Layout

#Step response of the spring system
\end_layout

\begin_layout Plain Layout

num1 = poly1d([0.1,0,0]);
\end_layout

\begin_layout Plain Layout

den1 = poly1d([1,0,1]);
\end_layout

\begin_layout Plain Layout

sys2 = signal.lti(num1,den1);
\end_layout

\begin_layout Plain Layout

y1 = sys2.step(T=t)[1];
\end_layout

\begin_layout Plain Layout

figure();
\end_layout

\begin_layout Plain Layout

subplot(2,1,1);
\end_layout

\begin_layout Plain Layout

plot(t,y1,'b');
\end_layout

\begin_layout Plain Layout

title(r"Displacement in 1D spring system using step response");
\end_layout

\begin_layout Plain Layout

xlabel(r"time$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"Displacement$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

#Impulse response of the spring system
\end_layout

\begin_layout Plain Layout

num2 = poly1d([0.1,0]);
\end_layout

\begin_layout Plain Layout

den2 = poly1d([1,0,1]);
\end_layout

\begin_layout Plain Layout

sys1 = signal.lti(num2,den2);
\end_layout

\begin_layout Plain Layout

y2 = sys1.impulse(T=t)[1];
\end_layout

\begin_layout Plain Layout

subplot(2,1,2);
\end_layout

\begin_layout Plain Layout

plot(t,y2,'r');
\end_layout

\begin_layout Plain Layout

title(r"Displacement in 1D spring system using impulse response");
\end_layout

\begin_layout Plain Layout

xlabel(r"time$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"Displacement$
\backslash
rightarrow$");
\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
SOLVING FOR THE 2D SPRING SYSTEM:
\end_layout

\begin_layout Standard
The 2D spring problem in hand is characterised by the equations:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ddot{x+(x-y)=0}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\ddot{y}+2(y-x)=0$
\end_inset


\end_layout

\begin_layout Standard
To solve for y, we substitute x from equation 2 in equation 1, to get a
 4
\begin_inset Formula $^{th}$
\end_inset

 degree equation in y.
 We then get the Laplace transform for y and solve for y(t) by getting the
 impulse response of Y(s).
 A similar procedure is used to solve for x(t).
\end_layout

\begin_layout LyX-Code
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
*
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

#2-d spring system
\end_layout

\begin_layout Plain Layout

#Solving for x
\end_layout

\begin_layout Plain Layout

num3 = poly1d([1,0,2]);
\end_layout

\begin_layout Plain Layout

den3 = poly1d([1,0,3,0]);
\end_layout

\begin_layout Plain Layout

sys3 = signal.lti(num3,den3);
\end_layout

\begin_layout Plain Layout

x3 = sys3.impulse(T=t)[1];
\end_layout

\begin_layout Plain Layout

figure();
\end_layout

\begin_layout Plain Layout

subplot(2,1,1);
\end_layout

\begin_layout Plain Layout

title(r"X Displacement in a 2D spring system");
\end_layout

\begin_layout Plain Layout

xlabel(r"time$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"x$
\backslash
rightarrow$")
\end_layout

\begin_layout Plain Layout

plot(t,x3);
\end_layout

\begin_layout Plain Layout

#Solving for y
\end_layout

\begin_layout Plain Layout

num4 = poly1d([2,0]);
\end_layout

\begin_layout Plain Layout

den4 = poly1d([1,0,3,0,0]);
\end_layout

\begin_layout Plain Layout

sys4 = signal.lti(num4,den4);
\end_layout

\begin_layout Plain Layout

y4 = sys4.impulse(T=t)[1];
\end_layout

\begin_layout Plain Layout

subplot(2,1,2);
\end_layout

\begin_layout Plain Layout

title(r"Y Displacement in a 2D spring system");
\end_layout

\begin_layout Plain Layout

xlabel(r"time$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"y$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

plot(t,y4);
\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
TRANSFER FUNCTION OF R-L CIRCUIT:
\end_layout

\begin_layout Standard
We solve for the following R-L circuit:
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/Screenshot from 2015-11-08 18:39:19.png
	scale 50

\end_inset


\end_layout

\begin_layout Standard
The transfer function is given by:
\end_layout

\begin_layout Standard
\begin_inset Formula $H(s)=\frac{100}{110+10^{-3}s}$
\end_inset


\end_layout

\begin_layout Standard
We implement the above in the program and study the phase and magnitude
 plots:
\end_layout

\begin_layout LyX-Code
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
*
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

#Response of the given RL circuit.
\end_layout

\begin_layout Plain Layout

R1 =10 
\end_layout

\begin_layout Plain Layout

R2 =100
\end_layout

\begin_layout Plain Layout

L = 0.001
\end_layout

\begin_layout Plain Layout

omega = logspace(3,9,61).reshape(61,1);#omega vector
\end_layout

\begin_layout Plain Layout

H = R2/((R1+R2)+1j*omega*L); #The transfer function
\end_layout

\begin_layout Plain Layout

#Plot w.r.t log(omega)
\end_layout

\begin_layout Plain Layout

#Magnitude plot of Transfer function
\end_layout

\begin_layout Plain Layout

figure();
\end_layout

\begin_layout Plain Layout

subplot(2,1,1);
\end_layout

\begin_layout Plain Layout

title(r"Magnitude Plot")
\end_layout

\begin_layout Plain Layout

xlabel(r"$
\backslash
Omega 
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"$
\backslash
mid$H$
\backslash
mid 
\backslash
rightarrow$")
\end_layout

\begin_layout Plain Layout

loglog(omega,abs(H),'ro');
\end_layout

\begin_layout Plain Layout

#Phase plot 
\end_layout

\begin_layout Plain Layout

subplot(2,1,2);
\end_layout

\begin_layout Plain Layout

title(r"Phase Plot")
\end_layout

\begin_layout Plain Layout

xlabel(r"$
\backslash
Omega 
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"$
\backslash
angle$H $
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

semilogx(omega,180*angle(H)/pi,'ro');
\end_layout

\begin_layout Plain Layout

#Plot w.r.t frequency
\end_layout

\begin_layout Plain Layout

frequency = omega/2/pi;
\end_layout

\begin_layout Plain Layout

figure();
\end_layout

\begin_layout Plain Layout

#Magnitude plot
\end_layout

\begin_layout Plain Layout

subplot(2,1,1);
\end_layout

\begin_layout Plain Layout

title(r"Magnitude Plot")
\end_layout

\begin_layout Plain Layout

xlabel(r"f $
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"$
\backslash
mid$H$
\backslash
mid 
\backslash
rightarrow$")
\end_layout

\begin_layout Plain Layout

loglog(frequency,abs(H),'ro');
\end_layout

\begin_layout Plain Layout

#Phase plot
\end_layout

\begin_layout Plain Layout

subplot(2,1,2);
\end_layout

\begin_layout Plain Layout

title(r"Phase Plot")
\end_layout

\begin_layout Plain Layout

xlabel(r"f $
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"$
\backslash
angle$H $
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

semilogx(frequency,180*angle(H)/pi,'ro');
\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
RESPONSE TO THE GIVEN INPUT:
\end_layout

\begin_layout Standard
The input voltage is given by:
\end_layout

\begin_layout Standard
\begin_inset Formula $v_{i}(t)=cos(10^{3}t)u(t)-cos(10^{6}t)u(t)$
\end_inset


\end_layout

\begin_layout Standard
We first find the Lapace Transform of the given function:
\end_layout

\begin_layout Standard
\begin_inset Formula $X(s)=\frac{s}{s^{2}+10^{6}}-\frac{s}{s^{2}+10^{12}}$
\end_inset


\end_layout

\begin_layout Standard
The output voltage is given by the impulse response of Y(s)=X(s)
\begin_inset Formula $\times$
\end_inset

H(s).
 This is implemented in the program and the behaviour of the system at different
 time intervals is studied.
\end_layout

\begin_layout LyX-Code
\begin_inset Flex Chunk
status open

\begin_layout Plain Layout

\begin_inset Argument 1
status open

\begin_layout Plain Layout
*
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

#Solving the system for given input
\end_layout

\begin_layout Plain Layout

t1 = linspace(0,0.01,pow(10,5)+1);
\end_layout

\begin_layout Plain Layout

#t1 = linspace(0,3*pow(10,-5),6001); #To examine transient response at the
 start
\end_layout

\begin_layout Plain Layout

num5 = poly1d([100*(pow(10,12)-pow(10,6)),0]);
\end_layout

\begin_layout Plain Layout

den5 = polymul(poly1d([1,0,pow(10,6)+pow(10,12),0,pow(10,18)]),poly1d([0.001,110]
));
\end_layout

\begin_layout Plain Layout

sys5 = signal.lti(num5,den5);
\end_layout

\begin_layout Plain Layout

y5 = sys5.impulse(T=t1)[1];
\end_layout

\begin_layout Plain Layout

figure();
\end_layout

\begin_layout Plain Layout

title(r"Time Response of the system to given input")
\end_layout

\begin_layout Plain Layout

xlabel(r"time$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

ylabel(r"Output voltage$
\backslash
rightarrow$");
\end_layout

\begin_layout Plain Layout

plot(t1,y5);
\end_layout

\begin_layout Plain Layout

show();
\end_layout

\end_inset


\end_layout

\begin_layout Part*
THE PLOTS:
\end_layout

\begin_layout Subsubsection*
The 1D Spring System:
\end_layout

\begin_layout Standard
The displacement is a sinuisodal curve as expected.
 Also, we get identical answers using either of the methods mentioned earlier.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_1.png
	scale 50

\end_inset


\end_layout

\begin_layout Subsubsection*
The 2D Spring system:
\end_layout

\begin_layout Standard
In the 2D spring system, the x and y displacements are seen to be out of
 phase, which is as expected.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_2.png
	scale 50

\end_inset


\end_layout

\begin_layout Subsubsection*
The transfer function of the given R-L circuit:
\end_layout

\begin_layout Standard
We can observe the sudden change in the magnitude plot of the transfer function
 at around 
\begin_inset Formula $\Omega=10^{5}$
\end_inset

, which as expected from the pole of the transfer function.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_3.png
	scale 50

\end_inset


\end_layout

\begin_layout Standard
Since the frequency is the more relevant physical quantity, we also plot
 the graphs against frequency.The inferences are the same as with previous
 plot.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_4.png
	scale 50

\end_inset


\end_layout

\begin_layout Subsubsection*
The Output Voltage corresponding to given Input Voltage:
\end_layout

\begin_layout Standard
We can observe that the output is formed by the superposition of the responses
 to the individual frequencies.
 This is as expected from the linearity property of the components present
 in the circuit.
 The plot looks like a thickened cosine-plot, but it is infact a cosine
 wave of a much smaller frequency being superimposed on the cosine wave
 with larger frequency.
 A zoomed in graph is shown in the next image.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_5.png
	scale 50

\end_inset


\end_layout

\begin_layout Standard
If we take a small time gap somewhere at in the time interval(Not at the
 very initial stage), we see that the response is similar to that of the
 
\begin_inset Formula $10^{3}$
\end_inset

 frequency(approximately).
 The response to 
\begin_inset Formula $10^{6}$
\end_inset

frequency is not apparent.
 It only provides a dc offset.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_7.png
	scale 50

\end_inset


\end_layout

\begin_layout Standard
However, we notice something very interesting in the very initial stages(t<30
\begin_inset Formula $\mu$
\end_inset

s).
 The circuit here has a transient behaviour, as the voltage across the resistor
 moves from 0 to 1.
 At a larger time, the circuit has reached a steady state and has a sinuisodal
 response.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename FIGURES/figure_6.png
	scale 50

\end_inset


\end_layout

\end_body
\end_document
