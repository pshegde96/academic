from pylab import *
import sys

#if arguments are passed correctly
if len(sys.argv) is 6 :
	n  = int(sys.argv[1]); #length of the tubelight
	nk = int(sys.argv[2]); #No of iterations done in the simulation
	u0 = int(sys.argv[3]); #Threshold velocity to collide and emit.
	p = float(sys.argv[4]); #Probability of collision
	M = int(sys.argv[5]); #mean no of electrons injected per iteration

#If arguments are not passed correctly
else : 
	n = 100;
	nk = 500;
	u0 = 5;
	p = 0.25;
	M = 10;

sigM = 2; #Standard deviation in no of injected electrons
a = 1; #Acceleration of electrions.
xx = array([-1 for i in range(0,M*n)],dtype = float32); #Store the position of electrons
u = array([-1 for i in range(0,M*n)]); #Store the velocity of electrons.
dx = array([0 for i in range(0,M*n)],dtype = float32); #Displacement in each time-step
I = []; #Stores the intensity population values
X = []; #Stores the position population values
V = []; #Stores the velocity population values

#Iterate over the nk time steps:
for k in range(0,nk):
	#Injecting electrons and initialising the variables accordingly
	m =  M + randn()*sigM;
	jj = array(where(xx ==-1));
	if(size(jj)>=m):	#Make sure there are atleast m free slots in xx.
		jj = jj[[0],0:M]; #Choose M of these elements to be added. 
		xx[jj[0]] = 0;	#xx = 0 indicates it has just been added
		u[jj[0]] = 0;
		dx[jj[0]] = 0;
	else:
		append(xx,[0,0,0,0,0]);
		append(u,[0,0,0,0,0]);
		append(dx,[0,0,0,0,0]);

	#Find elements that have crossed the length and reset them
	ik = array(where(xx>n));
	xx[ik[0]]= -1;
	u[ik[0]] = 0;
	dx[ik[0]] = 0;

	#Find the electrons with u>u0(energitic enough to collide)
	kk = array(where(u>u0));
	kk = kk[0];
	if (size(kk)>0):

		#Find the indices of kk which may collide.
		#Applying the p probability of collisions.
		r = random_sample((size(kk),));
		pp = array([p for i in range(0,size(kk))],dtype=float32);
		ll = array(where(r<=pp))
		#Find indices of the electrons that are going to collide.
		kl = kk[ll[0]];
		

	#Do the necessary operations on the electrons that collide:
		#It collindes at a random 'time' between the time-step
		#The random time gap is given by r.
		#Reset u and register xx at time = t+r.
		r = random_sample((size(kl),))
		xx[kl] = xx[kl] - u[kl]*r - a/2*r*r;
		u[kl] = 0;
		I.extend(xx[kl].tolist());
		#The remaining dist it travels in the remaining time.
		xx[kl]= xx[kl]+a/2*r*r;
		u[kl]= a*r;

	#Find elements within the length and change their position,velocity
	ii = array(where(xx>-1));
	X.extend(xx[ii[0]].tolist());
	V.extend(u[ii[0]].tolist());
	dx[ii[0]] = u[ii[0]]   + a/2;
	xx[ii[0]]= xx[ii[0]]+dx[ii[0]];
	u[ii[0]] = u[ii[0]]+a;	
#End loop.

#The Intensity Population plot	
title(r"Emmision intensity distribution")
xlabel(r"Position x $\rightarrow$")
ylabel(r"intensity $\rightarrow$")
value,bins,temp = hist(I,100,normed=1);
figure();

#The Position population plot
title(r"Population distribution")
xlabel(r"Position x $\rightarrow$")
ylabel(r"Probability $\rightarrow$")
hist(X,100,normed=1);
figure();

#The Phase Diagram
title(r"Phase Space Diagram:")
xlabel(r"Position x $\rightarrow$")
ylabel(r"Velcity v $\rightarrow$")
plot(X,V,'ro');
show();

#Printing the Intensity population plot in tabular form.
xpos = 0.5*(bins[0:-1]+bins[1:]);
print type(xpos);
print "Intensity Data:";
print "xpos \t count";
for i in range(0,size(xpos)):
	print "{0:.3f} 	{0:.3f}".format(xpos[i],value[i])
#End program.
	
	

	






	
