/***********************************************************************
*TITLE: SPICE CIRCUIT SIMULATOR
*AUTHOR: PARIKSHIT S HEGDE(EE14B123)
*DESCRIPTION:This program performs the function of a simple circuit simulator
* It can handle simple DC and AC RLC circuits.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#define MAXLENGTH 512
#define MAXNODES 20
#define MAXVAR 40
#define PI 3.142
#define DC 0
#define AC 1
#define OUTPUT_FILE "spice.out"
float w = 1000;

/*Global Variables*/

int circuit_status = 0; /*Tells whether the current line being read is part of the circuit description*/
char node_list[MAXNODES][10];/*Saves the list of unique nodes*/
int node_list_size=0;/*Saves the number of nodes*/
int node_map[MAXNODES][MAXNODES];/*To map the connection between nodes*/
complex cond_mat[MAXVAR][MAXVAR];/*The conductance/incidence matrix*/
int cond_mat_size = 0;/*Stores size of incidence matrix*/
complex v_vect[MAXVAR];/*The unknown voltage vector*/
complex i_vect[MAXVAR];/*The known current vector*/
char i_map[MAXVAR][5];/*To keep note of extra equations arising from V sources*/
char command[10][5]; /*To store the command line information*/
int i_map_size = 0;
void row_reduction();	/*Converts cond_mat to row reduced form*/
void solution_finder();	/*Finds the v_vect soln from row reduced cond_mat*/
void solution_printer();/*Prints solns on screen and in output file*/
int circuit_type = DC; /*DC or AC*/
double var_value = 0; /*Stores the variable source value to print in file*/
int test_counter = 0; /*To print the title in the file only 1ce,as function is called many times*/
static FILE* output_file;	/*Output file*/



/*Defining the structure of the double linked list*/
typedef struct Node{
	struct Node* prev;
	struct Node* next;
	char element[10];
	char n1[10];
	char n2[10];
	char n3[10];
	char n4[10];
	double value;
	int power;
}Node;

typedef struct DList{
	Node* head;
	Node* last;
	int list_count;
}DList;


Node* variable = NULL; /*Stores the node which is varying*/
/*Function prototypes,function explained later*/
void primary(DList*);  
void element_stamp(Node*);

/*add() function adds a new node to the list*/
void add(Node* new,DList* list)
{
	if(list->list_count==0){
		list->head = new;
		list->list_count++;	
	}
	else {
		new->next = list->head;
		list->head->prev = new;
		
		list->head=new;
		list->list_count++;
	}
}

/*Function to find unique nodes and add it to node_list*/
void node_adder(Node* node)
{
	int i,flag1 = 0,flag2 = 0;
	for(i=0;i<node_list_size;i++){
		if(strcmp(node->n1,node_list[i])==0)
			flag1 =1;
		if(strcmp(node->n2,node_list[i])==0)
			flag2 =1;
	}
	if(flag1 == 0){
		strcpy(node_list[node_list_size],node->n1);	
		node_list_size +=1;
	}
	if(flag2==0){
		strcpy(node_list[node_list_size],node->n2);
		node_list_size += 1;
	}
	
	/*When element has 4 nodes*/
	if(strcmp("\0",node->n3)!=0){
		int flag3=0,flag4=0;
		
			
		for(i=0;i<node_list_size;i++){
			if(strcmp(node->n3,node_list[i])==0)
				flag3 =1;
			if(strcmp(node->n4,node_list[i])==0)
				flag4 =1;
		}
		if(flag3 == 0){
			strcpy(node_list[node_list_size],node->n3);	
			node_list_size +=1;
		}
		if(flag4==0){
			strcpy(node_list[node_list_size],node->n4);
			node_list_size += 1;
		}
	}	
		
}
/*Function to incorporate the power into the element value*/
double value(char s[10])
{	
	int p=0;
	double val;
	char power[5];
	sscanf(s,"%lf%s",&val,power);
	switch (power[0]){
			case 'u': p = -6;break;
			case 'm': p = -3;break;
			case 'n': p = -9;break;
			case 'k': p = 3;break;
			case 'p': p = -12;break;
			case '\0':p = 0; break;
			default: printf("Invalid Power in command line\n\n");
				 exit(1);
			}		
	if(strcmp(power,"meg")==0)
		p = 6;

	val = val*pow(10,p);
	
	return (val);
	
}
/*Function used to free the list after the program is done*/
list_free(DList* list)
{
	if(list->list_count>0){
		list->head = list->head->next;
		list->list_count--;
		list_free(list);
	}
	free(list->head);
}



/*The MAIN function*/
int main(int argc,char** argv)
{
	output_file = fopen(OUTPUT_FILE,"w");
	if(output_file==NULL)
	{
		printf("output file could not be opened, output will just be printed on screen");
	}
	/*Initiallising the the double linked list*/	
	
	DList* list = (DList*) malloc(sizeof(DList));
	
	assert(list != NULL);
	list->head = NULL;
	
	list->list_count = 0;
	/*End of initiallisation of dll*/

	/*Reading in the spice netlist file*/
	FILE *fp = fopen(argv[1],"r");
	if(fp==NULL){
		printf("Could not open the file,exiting");
		exit(1);
	}

	char buff[MAXLENGTH];
	/*Reading the file line by line*/
	while(fgets(buff,MAXLENGTH,fp))
	{	
		/*Checking whether the line is a part of ckt description*/
		char checker[10];
		sscanf(buff,"%s",checker);
		
		
		
		if(strcmp(checker,".end")==0){
		/*If in commad mode,go back to ckt description mode*/
		/*If in ckt description mode, go to off mode*/
			circuit_status = circuit_status-1;	
			
			}
		if(strcmp(checker,".circuit")==0){
			circuit_status = 1;
				
			}	
		/*Ignore the line if not part of ckt description*/
		if(circuit_status==0)
			continue;
		if(strcmp(checker,".command")==0){
			circuit_status = 2;	
		}
		if((checker[0]=='.')||(checker[0]=='\0'))
			continue;
		if(circuit_status==1){
			/*Node to save the details of the element and add it to list*/
			Node* new = (Node*) malloc(sizeof(Node));
			assert(new!=NULL);	
		
		
			char str1[10],str2[10],str3[10],str4[10],str5[10],str6[10],pow[4]="\0";
		
			/*Check 1st character for element type and proceed*/
			switch (buff[0]){

				case 'R':
				case 'L':
				case 'C':
				case 'V':
				case 'I':
					sscanf(buff,"%s %s %s %s",str1,str2,str3,str4);
				
					strcpy(new->element,str1);
					strcpy(new->n1,str2);
					strcpy(new->n2,str3);
					strcpy(new->n3,"\0"); /*To say it has only 2 nodes*/
					sscanf(str4,"%lf%s",&(new->value),pow);
					new->next = NULL;
					new->prev = NULL;
					break;	
			
				case 'E':
				case 'G':
				case 'H':
				case 'F':
					sscanf(buff,"%s %s %s %s %s %s",str1,str2,str3,str4,str5,str6);
					strcpy(new->n1,str2);
					strcpy(new->element,str1);
					strcpy(new->n2,str3);
					strcpy(new->n3,str4);
					strcpy(new->n4,str5);
					sscanf(str6,"%lf%s",&(new->value),pow);
					break;

				default :
					printf("Invalid element\n");
					continue;
				}
				/*Assigning proper value for power*/
				switch (pow[0]){
					case 'u': new->power = -6;break;
					case 'm': new->power = -3;break;
					case 'n': new->power = -9;break;
					case 'k': new->power = 3;break;
					case 'p': new->power = -12;break;
					case '\0': new->power = 0; break;
					default: printf("Invalid Power for %s\n",str1);
				}		
			if(strcmp(pow,"meg")==0)
				new->power = 6;
	
			/*Add the newly described element to the list*/
			add(new,list);
		}
		/*If in command mode,store the line in diff strings, later make sense of it*/
		if(circuit_status==2)
		{
			sscanf(buff,"%s %s %s %s %s",command[0],command[1],command[2],command[3],command[4]);
		}
	}
	
	

	int i,j;
	/*To find the list of unique nodes*/
	Node *node = list->head;
	for(i=1;i<=list->list_count;i++){
		if(i<list->list_count)
		node = node->next;
		node_adder(node);	
	}
	
	list->last = node;
	
	node = list->head; 

	/*Incorporating the power in the element values*/
	for(i=1;i<=list->list_count;i++){
		node->value = node->value * pow(10,node->power);
		node=node->next;
	}	
	
	node = list->head;
	

	if(strcmp(command[0],"dc")==0)
	{	circuit_type = DC;
		/*Find out which is the variable element*/
		for(i=0;i<list->list_count;i++)
		{
			if(strcmp(command[1],node->element)==0){
				variable = node;
				break;
			}
			node=node->next;
		}
		/*Find out v_min,v_max and step(same for currents)*/
		double v_min,v_max,v_step;
		v_min = value(command[2]);
		v_max = value(command[3]);
		v_step = value(command[4]);
		printf("\n\n");
		/*Loop counter*/
		double lc;
		/*Go through all the voltage values*/
		for(lc = v_min;lc<=v_max;lc+=v_step)
		{
			variable->value = lc;
			var_value = lc;
			
			primary(list);	
			printf("\n\n\n\n\n");
		}
		
	}

	else if(strcmp(command[0],"ac")==0)
	{	
		circuit_type = AC;
		/*Find out variable element(However this is not used in our program)*/
		for(i=0;i<list->list_count;i++)
		{
			if(strcmp(command[1],node->element)==0){
				variable = node;
				break;
			}
			node=node->next;
		}
		/*Find out f_min,f_max and step voltages*/
		double f_min,f_max,n,lf_min,lf_max,lstep,counter;
		f_min = value(command[2]);
		f_max = value(command[3]);
		n = atof(command[4]);
		lf_min = log(f_min);
		lf_max = log(f_max);
		lstep = (double) log(10.0)/n;
		
		/*Go through all the step frequencies*/	
		for(counter = lf_min;counter<=lf_max;counter+=lstep)
		{
			w = 2*PI* exp(counter);
			primary(list);	
			printf("\n\n\n\n\n");
		}
		/*In the above for loop, due to round-off errors last value is not considered*/
		w = 2*PI*exp(counter);
		primary(list);	
		printf("\n\n\n");
	}

	/*When there is no command line,just call primary function*/
	else
		primary(list);
	/*Free all the list memory and close the files*/
	list_free(list);
	fclose(fp);	
	fclose(output_file);
}

/*To insert the element stamp in the conductance matrix*/
void element_stamp(Node* node)
{	int n1,n2,n3,n4,i;
	
	for(i = 0;i<node_list_size;i++)
	{
		if(strcmp(node_list[i],(node->n1))==0)
			n1 = i;

		if(strcmp(node_list[i],(node->n2))==0)
			n2 = i;

		if(strcmp(node->n3,"\0"))
		{
			if(strcmp(node_list[i],(node->n3))==0)
				n3 = i;

			if(strcmp(node_list[i],(node->n4))==0)
				n4 = i;
		}
	}

	switch (node->element[0])
	{	
		case 'R':
			cond_mat[n1][n1] +=  1/(node->value); 
			cond_mat[n2][n2] +=  1/(node->value) ;
			cond_mat[n1][n2] +=  -1/(node->value);
			cond_mat[n2][n1] +=  -1/(node->value);
			break;

		case 'I':
			i_vect[n1] +=  -1*(node->value);
			i_vect[n2] +=  1*(node->value);
			break;

		case 'V':
			strcpy(i_map[i_map_size],(node->element));
			i_map_size++;
			cond_mat[n1][cond_mat_size] +=1;
			cond_mat[n2][cond_mat_size] +=-1;
			cond_mat[cond_mat_size][n1] += 1;
			cond_mat[cond_mat_size][n2] += -1;
			i_vect[cond_mat_size] += node->value; 
			cond_mat_size++;
			break;

		case 'C':
			cond_mat[n1][n1] +=  I*w*(node->value); 
			cond_mat[n2][n2] +=  I*w*(node->value) ;
			cond_mat[n1][n2] +=  -I*w*(node->value);
			cond_mat[n2][n1] +=  -I*w*(node->value);			
			break;

		case 'L':
			cond_mat[n1][n1] +=  -I/w/(node->value); 
			cond_mat[n2][n2] +=  -I/w/(node->value) ;
			cond_mat[n1][n2] +=  I/w/(node->value);
			cond_mat[n2][n1] +=  I/w/(node->value);			
			break;
		
	}	
	
}
/*This function takes the conductance matrix and converts it to row-reduced form*/
/*If the matrix is invertible, then each row should be left with only one 1,rest all 0s*/

void row_reduction()
{
	int row,col,k,l;
	complex fne,multiple;
	
	for(row=0;row<cond_mat_size;row++){
		for(col=0;col<cond_mat_size;col++)
		{	/*Find first non-zero element in the row*/
			if(cond_mat[row][col]!=0){
				fne = cond_mat[row][col];
				/*Make the first non-zero element of the row to 1*/
				for(k=col;k<cond_mat_size;k++)
					cond_mat[row][k] = cond_mat[row][k]/fne;
				i_vect[row]=i_vect[row]/fne;
				/*Make all the elements in the column of fne 0*/	
				for(k=0;k<cond_mat_size;k++){
					if(k!=row){
						multiple = cond_mat[k][col];
						i_vect[k] = i_vect[k] - multiple*i_vect[row];
						for(l=0;l<cond_mat_size;l++)
							cond_mat[k][l]=cond_mat[k][l]-multiple*cond_mat[row][l];			 
					}
				}
				break;
			}
		}
	}
}
/*This function takes in the row-reduced matrix and i_vect to find unknown v_vect*/
void solution_finder()
{
	int row,col;
	for(row=0;row<cond_mat_size;row++)
		for(col=0;col<cond_mat_size;col++){
			if(cond_mat[row][col]!=0){
				v_vect[col] = i_vect[row];
				break;
			}
		}
}
/*This function prints out the solution on screen*/
void solution_printer(DList *list)
{	
	Node* node = list->head;
	int list_count = list->list_count;
	int i;
	/*Printing node voltages and voltage currents*/
	printf("The solution is:\n\n");
	for(i=0;i<cond_mat_size;i++){
		if(i<node_list_size)
			printf("Voltage of %s = %f %+fi \n",node_list[i],creal(v_vect[i]),cimag(v_vect[i]));
		else
			printf("Current through %s = %f %+fi \n",i_map[i-cond_mat_size],creal(v_vect[i]),cimag(v_vect[i]));
	}
	/*Printing out the voltage across and current through each element*/
	printf("\n\nThe element relations are:\n\n");
	
	for(i=list->list_count;i>0;i--)
	{	int n1,n2,j;
		complex current,voltage;
		/*Find nodes across which element is present*/
		for(j = 0;j<node_list_size;j++)
		{
			if(strcmp(node_list[j],(node->n1))==0)
				n1 = j;

			if(strcmp(node_list[j],(node->n2))==0)
				n2 = j;			
		}
		/*Find voltage and current according to the element properties*/
		switch (node->element[0])
		{
			case 'R':
				voltage = v_vect[n1]-v_vect[n2];
				current = voltage/(node->value);		
				break;
	
			case 'V':
				voltage = v_vect[n1] - v_vect[n2];
				for(j=0;j<cond_mat_size-node_list_size;j++)
					if(strcmp(i_map[j],node->element)==0)
						current = v_vect[j+node_list_size];
				break;

			case 'C':
				voltage =  v_vect[n1]-v_vect[n2];	
				current = voltage*I*w*(node->value);
				break;
			case 'L':
				voltage = v_vect[n1]-v_vect[n2];
				current = -1.0*voltage*I/w/(node->value);
				break;
		
		}
		/*Printing out current and voltage results of elements*/
		printf("Element %s:\n",node->element);
		printf("Current = %.4f %+.4fi\n",creal(current),cimag(current));
		printf("Voltage = %.4f %+.4fi\n\n",creal(voltage),cimag(voltage));
		
				
		node = node->next;
	}
}
/*Once all the information is gathered by the main function, this function does everything from*
* calling functions to find the solution of the v_vect, and then printingo out the results int *
* output file*/

void primary(DList* list)
{	int i,j;	/*Loop counters*/
	Node* node = list->head;
	/*Initial conductance matrix and i_vect to 0*/
	for(i=0;i<MAXVAR;i++){
		for(j=0;j<MAXVAR;j++)	
			cond_mat[i][j] = 0;
		i_vect[i]=0;
		v_vect[i]=0;
	}

	cond_mat_size = node_list_size;
	i_map_size = 0;

	
	/*Adding element stamps to the conduction matrix*/
	for(i=0;i<list->list_count;i++){
		element_stamp(node);
		node = node->next;
	}
	
	/*Finding ground node and setting it to 0*/
	int ng;	
	for(i=0;i<node_list_size;i++)
		if(strcmp(node_list[i],"GND")==0){
			ng = i;
			break;	
		}

	for(i=0;i<cond_mat_size;i++){
		if(ng==i)
			cond_mat[ng][ng]=1;
		else{
			cond_mat[i][ng]=0;
			cond_mat[ng][i]=0;
		}
	}
	i_vect[ng]=0;	
	
	row_reduction();	/*Reduces cond_mat to row-reduced form*/	
	solution_finder();	/*Finds soln for v_vect*/
	solution_printer(list);	/*Prints soln of v_vect on screen*/
	/*If the output file is open, then it prints the soln in the output file*/
	if(output_file!=NULL)
	{
		if(circuit_type == DC)
		{	
			char temp[100]; /*Holds the line of the file till it is printed in the file*/
			/*The first column is the variable element value*/
			sprintf(temp,"%.3lf \t ",variable->value);
			int i;
			/*Putting the node voltages and Vcurrents in according columns*/
			for(i=0;i<cond_mat_size;i++)
			{
				char s[10];
				if(cimag(v_vect[i])==0)
					sprintf(s,"%.3lf \t",creal(v_vect[i]));
				else
					sprintf(s,"%.4lfcis(%.2lf) \t",cabs(v_vect[i]),carg(v_vect[i]));

				strcat(temp,s);
			}
			strcat(temp,"\n");

			/*The first row has the headings of the columns*/
			if(test_counter==0){
				char temp1[100]="",s[10];
				sprintf(s,"%s \t",variable->element);
				strcat(temp1,s);
	
				for(i=0;i<node_list_size;i++){
					sprintf(s,"V_%s \t",node_list[i]);
					strcat(temp1,s);
				}
				for(i=0;i<i_map_size;i++){
					sprintf(s,"I_%s \t",i_map[i]);
					strcat(temp1,s);
				}
				strcat(temp1,"\n");
				fprintf(output_file,"%s","#All values are in SI units\n");
				fprintf(output_file,"%s",temp1);
				test_counter++;
				
			}
			fprintf(output_file,"%s",temp);
		}

		else if(circuit_type == AC)
		{
			char temp[100];/*Holds a line of the line till it is printed onto the file*/
			
			/*The first column is the values of frequencies*/			
			sprintf(temp,"%lf \t ",w/2/PI);
			int i;
			/*Putting node voltages and Icurrents accordingly*/
			for(i=0;i<cond_mat_size;i++)
			{
				char s[10];
				sprintf(s,"%.4lfcis(%.2lf) \t",cabs(v_vect[i]),carg(v_vect[i]));
				strcat(temp,s);
			}
			strcat(temp,"\n");

			/*First row contains the headings of the columns*/
			if(test_counter==0){
				char temp1[100]="",s[10];
				sprintf(s,"%s \t \t \t","f");
				strcat(temp1,s);
	
				for(i=0;i<node_list_size;i++){
					sprintf(s,"V_%s \t \t \t",node_list[i]);
					strcat(temp1,s);
				}
				for(i=0;i<i_map_size;i++){
					sprintf(s,"I_%s \t \t \t",i_map[i]);
					strcat(temp1,s);
				}
				strcat(temp1,"\n");
				fprintf(output_file,"%s","#All values are in SI units\n");
				fprintf(output_file,"%s",temp1);
				test_counter++;
			}
			fprintf(output_file,"%s",temp);
		}
	}	
}
