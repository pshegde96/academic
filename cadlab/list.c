#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define MAXLENGTH 512

/*Global Variables*/

int circuit_status = 0; /*Tells whether the current line being read is part of the circuit description*/

/*Defining the structure of the double linked list*/
typedef struct Node{
	
	struct Node* next;
	char element[10];
	char n1[10];
	char n2[10];
	char n3[10];
	char n4[10];
	float value;
	int power;
}Node;

typedef struct DList{
	Node* head;

	int list_count;
}DList;



/*add() function adds a new node to the list*/
void add(Node* new,DList* list)
{
	new->next = list->head;
	list->head = new;
	list->list_count +=1;
}






/*The MAIN function*/
int main(int argc,char** argv)
{
	
	/*Initiallising the the double linked list*/	
	
	DList* list = (DList *) malloc(sizeof(DList));
	
	assert(list != NULL);
	list->head = NULL;
	
	list->list_count = 0;
	/*End of initiallisation of dll*/

	/*Reading in the spice netlist file*/
	FILE *fp = fopen(argv[1],"r");
	if(fp==NULL){
		printf("Could not open the file,exiting");
		exit(1);
	}

	char buff[MAXLENGTH];

	while(fgets(buff,MAXLENGTH,fp))
	{	
		
		char checker[10];
		sscanf(buff,"%s",checker);
		
		
		
		if(strcmp(checker,".end")==0){
			circuit_status = 0;	
			
			}
		if(strcmp(checker,".circuit")==0){
			circuit_status = 1;
				
			}	

		if(circuit_status==0)
			continue;
		if(strcmp(checker,".circuit")==0)
			continue;
		Node* new = (Node *) malloc(sizeof(Node));
			
		assert(new!=NULL);	
		
		
		char str1[10],str2[10],str3[10],str4[10],str5[10],str6[10];
		
			
		switch (buff[0]){

			case 'R':
			case 'L':
			case 'C':
			case 'V':
			case 'I':
				sscanf(buff,"%s %s %s %s",str1,str2,str3,str4);
				
				strcpy(new->element,str1);
				strcpy(new->n1,str2);
				strcpy(new->n2,str3);
				strcpy(new->n3,"\0");
				new->value = atof(str4);
				new->next = NULL;
				
				//new->power = 
				break;	
			
			case 'E':
			case 'G':
			case 'H':
			case 'F':
				sscanf(buff,"%s %s %s %s %s %s",str1,str2,str3,str4,str5,str6);
				strcpy(new->n1,str2);
				strcpy(new->element,str1);
				
				strcpy(new->n2,str3);
				strcpy(new->n3,str4);
				strcpy(new->n4,str5);
				new->value = atof(str6);
				//new->power
				break;
			default :
				printf("Invalid element");
				
		
			}
		printf("%s %s %s %f \n",new->element,new->n1,new->n2,new->value);
				add(new,list);
	}
	
	
	int i;
	
	/*Printing the list in reverse order*/
	printf("\nThe list of elements in reverse order is: \n\n");
	Node *node = list->head;
	for(i=1;i<=list->list_count;i++){
		printf("%s \t %s \t %s \t %f \n",node->element,node->n1,node->n2,node->value);
		
	
		node = node->next;	
	}

	/*Printing the list in reverse order*/
	printf("\nThe list of elements in reverse order is:\n\n");
	
	for(i =1;i<=list->list_count;i++){
		//output_reverse_order(node1,i);
	}
}
