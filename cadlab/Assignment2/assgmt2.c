#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef struct Node{				/* A node in the linked list which stores its own information and the location of the previous 							   node*/
	struct Node *next;
	int a;
	int b;
	}Node;

typedef struct List{				/*This structure keeps note of the current node in the list and the list count*/
	Node *head;
	int count;
	}List;

void add(List *list,Node *node)			/*A function used to add a node to the list*/
{
	node->next = list->head;
	list->head = node;
	list->count +=1;
}

void recursive_printer(List *list)		/*Recursive function used to print the list in reverse as well as correct order*/
{	int a,b;
	if(list->count>0)		
	{	
		a = list->head->a;
		b= list->head->b;
		list->count -= 1;
		list->head = list->head->next;
		printf("%d %d \n",a,b);		//This prints out the list in reverse order as we go through the list
		recursive_printer(list);
	
	}
	else 
	{	
		printf("\n \n The output in the correct order is: \n");
		return;
	}
	printf("%d %d \n",a,b);      //This prints out the list in correct order as recursive function traces back
	free(list->head);
}

void main(){

	int list_size;		/*list_size stores the size of the list*/
	
	List *list = malloc(sizeof(List));		/*Allocating space for the list object*/
	list->head =  NULL;				/*Setting initial values to null and 0*/
	list->count = 0;
	printf("Please enter the size of the list:");
	scanf("%d",&list_size);

	while(list->count<list_size)
	{
		Node *node = (Node *) malloc(sizeof(Node));	/*Allocating space for an object node and accepting its values*/
		assert (node != NULL);
		printf("Enter the two numbers : ");
		scanf("%d %d",&(node->a),&(node->b));
		
		node->next = NULL;
		add(list,node);		/*Passing the node to the add function to add it to the linked list*/
	}
	
	printf("\n");	

	

	printf("The output in reverse order is: \n");
	recursive_printer(list);	/*Calling the printing function to print the list in both reverse and correct order*/
}




