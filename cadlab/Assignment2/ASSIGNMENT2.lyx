#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
ASSIGNMENT 2
\end_layout

\begin_layout Author
PARIKSHIT S HEGDE(EE14B123)
\end_layout

\begin_layout Section*
Description of the program:
\end_layout

\begin_layout Standard
This program accepts the size of the list and the list of numbers and stores
 it in the form of a linked list.
 Later, it prints out the linked list in the both the reverse order and
 the correct order without using an additional linked list or array.
 The concept of recursive functions has been used to print out the numbers.
\end_layout

\begin_layout Section*
The program:
\end_layout

\begin_layout LyX-Code

\color blue
#include <stdio.h>
\end_layout

\begin_layout LyX-Code

\color blue
#include <stdlib.h>
\end_layout

\begin_layout LyX-Code

\color blue
#include <assert.h>
\end_layout

\begin_layout LyX-Code

\color blue
typedef struct Node{				
\color red
/* A node in the linked list which stores its own                      
                           information and the location of the previous
 node*/
\end_layout

\begin_layout LyX-Code

\color blue
	struct Node *next;
\end_layout

\begin_layout LyX-Code

\color blue
	int a;
\end_layout

\begin_layout LyX-Code

\color blue
	int b;
\end_layout

\begin_layout LyX-Code

\color blue
	}Node;
\end_layout

\begin_layout LyX-Code

\color blue
typedef struct List{				
\color red
/*This structure keeps note of the current node in the                 
                           list and the list count*/
\end_layout

\begin_layout LyX-Code

\color blue
	Node *head;
\end_layout

\begin_layout LyX-Code

\color blue
	int count;
\end_layout

\begin_layout LyX-Code

\color blue
	}List;
\end_layout

\begin_layout LyX-Code

\color blue
void add(List *list,Node *node)			
\color red
/*A function used to add a node to the list*/
\end_layout

\begin_layout LyX-Code

\color blue
{
\end_layout

\begin_layout LyX-Code

\color blue
	node->next = list->head;
\end_layout

\begin_layout LyX-Code

\color blue
	list->head = node;
\end_layout

\begin_layout LyX-Code

\color blue
	list->count +=1;
\end_layout

\begin_layout LyX-Code

\color blue
}
\end_layout

\begin_layout LyX-Code
void recursive_printer(List *list)		
\color red
/*Recursive function used to print the list in                         
                          reverse as well as correct order*/
\end_layout

\begin_layout LyX-Code
{	int a,b;
\end_layout

\begin_layout LyX-Code
	if(list->count>0)		
\end_layout

\begin_layout LyX-Code
	{	
\end_layout

\begin_layout LyX-Code
		a = list->head->a;
\end_layout

\begin_layout LyX-Code
		b= list->head->b;
\end_layout

\begin_layout LyX-Code
		list->count -= 1;
\end_layout

\begin_layout LyX-Code
		list->head = list->head->next;
\end_layout

\begin_layout LyX-Code
		printf("%d %d 
\backslash
n",a,b);		
\color red
/*This prints out the list in reverse order as we go                   
                           through the list*/
\end_layout

\begin_layout LyX-Code
		recursive_printer(list);
\end_layout

\begin_layout LyX-Code
	
\end_layout

\begin_layout LyX-Code
	}
\end_layout

\begin_layout LyX-Code
	else 
\end_layout

\begin_layout LyX-Code
	{	
\end_layout

\begin_layout LyX-Code
		printf("
\backslash
n 
\backslash
n The output in the correct order is: 
\backslash
n");
\end_layout

\begin_layout LyX-Code
		return;
\end_layout

\begin_layout LyX-Code
	}
\end_layout

\begin_layout LyX-Code
	printf("%d %d 
\backslash
n",a,b);      
\color red
/*This prints out the list in correct order as recursive               
                           function traces back*/
\end_layout

\begin_deeper
\begin_layout LyX-Code
free(list->head);
\end_layout

\end_deeper
\begin_layout LyX-Code
}
\end_layout

\begin_layout LyX-Code
void main(){
\end_layout

\begin_layout LyX-Code
	int list_size;		
\color red
/*list_size stores the size of the list*/
\end_layout

\begin_layout LyX-Code

\color red
	
\end_layout

\begin_layout LyX-Code
	List *list = malloc(sizeof(List));		
\color red
/*Allocating space for the list object*/
\end_layout

\begin_layout LyX-Code
	list->head =  NULL;				
\color red
/*Setting initial values to null and 0*/
\end_layout

\begin_layout LyX-Code
	list->count = 0;
\end_layout

\begin_layout LyX-Code
	printf("Please enter the size of the list:");
\end_layout

\begin_layout LyX-Code
	scanf("%d",&list_size);
\end_layout

\begin_layout LyX-Code
	while(list->count<list_size)
\end_layout

\begin_layout LyX-Code
	{
\end_layout

\begin_layout LyX-Code
		
\color blue
Node *node = (Node *) malloc(sizeof(Node));	
\color red
/*Allocating space for an object node                                  
                           and accepting its values*/
\end_layout

\begin_layout LyX-Code

\color blue
		assert (node != NULL);
\end_layout

\begin_layout LyX-Code

\color blue
		printf("Enter the two numbers : ");
\end_layout

\begin_layout LyX-Code

\color blue
		scanf("%d %d",&(node->a),&(node->b));
\end_layout

\begin_layout LyX-Code

\color blue
		
\end_layout

\begin_layout LyX-Code

\color blue
		node->next = NULL;
\end_layout

\begin_layout LyX-Code
		add(list,node);		
\color red
/*Passing the node to the add function to add it to the linked         
                           list*/
\end_layout

\begin_layout LyX-Code
	}
\end_layout

\begin_layout LyX-Code
	
\end_layout

\begin_layout LyX-Code
	printf("
\backslash
n");	
\end_layout

\begin_layout LyX-Code
	
\end_layout

\begin_layout LyX-Code
	printf("The output in reverse order is: 
\backslash
n");
\end_layout

\begin_layout LyX-Code
	recursive_printer(list);	
\color red
/*Calling the printing function to print the list in both              
                           reverse and correct order*/
\end_layout

\begin_layout LyX-Code
}
\end_layout

\begin_layout LyX-Code

\end_layout

\end_body
\end_document
