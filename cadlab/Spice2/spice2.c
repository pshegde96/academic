/***********************************************************************
*TITLE: SPICE CIRCUIT SIMULATOR
*AUTHOR: PARIKSHIT S HEGDE(EE14B123)
*DESCRIPTION:This program read from a spice netlist and forms the conductance
* matrix which will be solved in the next program.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#define MAXLENGTH 512
#define MAXNODES 20
#define MAXVAR 40
const float w = 100*22/7;

/*Global Variables*/

int circuit_status = 0; /*Tells whether the current line being read is part of the circuit description*/
char node_list[MAXNODES][10];/*Saves the list of unique nodes*/
int node_list_size=0;/*Saves the number of nodes*/
int node_map[MAXNODES][MAXNODES];/*To map the connection between nodes*/
complex cond_mat[MAXVAR][MAXVAR];/*The conductance/incidence matrix*/
int cond_mat_size = 0;/*Stores size of incidence matrix*/
complex v_vect[MAXVAR];/*The unknown voltage vector*/
complex i_vect[MAXVAR];/*The known current vector*/
char i_map[MAXVAR][5];/*To keep note of extra equations arising from V sources*/
int i_map_size = 0;


/*Defining the structure of the double linked list*/
typedef struct Node{
	struct Node* prev;
	struct Node* next;
	char element[10];
	char n1[10];
	char n2[10];
	char n3[10];
	char n4[10];
	double value;
	int power;
}Node;

typedef struct DList{
	Node* head;
	Node* last;
	int list_count;
}DList;

void element_stamp(Node*);//prototype

/*add() function adds a new node to the list*/
void add(Node* new,DList* list)
{
	if(list->list_count==0){
		list->head = new;
		list->list_count++;	
	}
	else {
		new->next = list->head;
		list->head->prev = new;
		
		list->head=new;
		list->list_count++;
	}
}

/*Function to find unique nodes and add it to node_list*/
void node_adder(Node* node)
{
	int i,flag1 = 0,flag2 = 0;
	for(i=0;i<node_list_size;i++){
		if(strcmp(node->n1,node_list[i])==0)
			flag1 =1;
		if(strcmp(node->n2,node_list[i])==0)
			flag2 =1;
	}
	if(flag1 == 0){
		strcpy(node_list[node_list_size],node->n1);	
		node_list_size +=1;
	}
	if(flag2==0){
		strcpy(node_list[node_list_size],node->n2);
		node_list_size += 1;
	}
	
	/*When element has 4 nodes*/
	if(strcmp("\0",node->n3)!=0){
		int flag3=0,flag4=0;
		
			
		for(i=0;i<node_list_size;i++){
			if(strcmp(node->n3,node_list[i])==0)
				flag3 =1;
			if(strcmp(node->n4,node_list[i])==0)
				flag4 =1;
		}
		if(flag3 == 0){
			strcpy(node_list[node_list_size],node->n3);	
			node_list_size +=1;
		}
		if(flag4==0){
			strcpy(node_list[node_list_size],node->n4);
			node_list_size += 1;
		}
	}	
		
}


list_free(DList* list)
{
	if(list->list_count>0){
		list->head = list->head->next;
		list->list_count--;
		list_free(list);
	}
	free(list->head);
}



/*The MAIN function*/
int main(int argc,char** argv)
{
	
	/*Initiallising the the double linked list*/	
	
	DList* list = (DList*) malloc(sizeof(DList));
	
	assert(list != NULL);
	list->head = NULL;
	
	list->list_count = 0;
	/*End of initiallisation of dll*/

	/*Reading in the spice netlist file*/
	FILE *fp = fopen(argv[1],"r");
	if(fp==NULL){
		printf("Could not open the file,exiting");
		exit(1);
	}

	char buff[MAXLENGTH];
	/*Reading the file line by line*/
	while(fgets(buff,MAXLENGTH,fp))
	{	
		/*Checking whether the line is a part of ckt description*/
		char checker[10];
		sscanf(buff,"%s",checker);
		
		
		
		if(strcmp(checker,".end")==0){
			circuit_status = 0;	
			
			}
		if(strcmp(checker,".circuit")==0){
			circuit_status = 1;
				
			}	
		/*Ignore the line if not part of ckt description*/
		if(circuit_status==0)
			continue;
		if((checker[0]=='.')||(checker[0]=='\0'))
			continue;

		/*Node to save the details of the element and add it to list*/
		Node* new = (Node*) malloc(sizeof(Node));
		assert(new!=NULL);	
		
		
		char str1[10],str2[10],str3[10],str4[10],str5[10],str6[10],pow[4]="\0";
		
		/*Check 1st character for element type and proceed*/
		switch (buff[0]){

			case 'R':
			case 'L':
			case 'C':
			case 'V':
			case 'I':
				sscanf(buff,"%s %s %s %s",str1,str2,str3,str4);
				
				strcpy(new->element,str1);
				strcpy(new->n1,str2);
				strcpy(new->n2,str3);
				strcpy(new->n3,"\0"); /*To say it has only 2 nodes*/
				sscanf(str4,"%lf%s",&(new->value),pow);
				new->next = NULL;
				new->prev = NULL;
				//new->power = 
				break;	
			
			case 'E':
			case 'G':
			case 'H':
			case 'F':
				sscanf(buff,"%s %s %s %s %s %s",str1,str2,str3,str4,str5,str6);
				strcpy(new->n1,str2);
				strcpy(new->element,str1);
				
				strcpy(new->n2,str3);
				strcpy(new->n3,str4);
				strcpy(new->n4,str5);
				sscanf(str6,"%lf%s",&(new->value),pow);
				
				break;
			default :
				printf("Invalid element\n");
				continue;
			}
			/*Assigning proper value for power*/
			switch (pow[0]){
				case 'u': new->power = -6;break;
				case 'm': new->power = -3;break;
				case 'n': new->power = -9;break;
				case 'k': new->power = 3;break;
				case 'p': new->power = -12;break;
				case '\0': new->power = 0; break;
				default: printf("Invalid Power for %s\n",str1);
			}		
			if(strcmp(pow,"meg")==0)
				new->power = 6;
	
			/*Add the newly described element to the list*/
			add(new,list);
	}
	
	
	int i,j;	/*Loop counters*/
	
	
	Node *node = list->head;
	for(i=1;i<=list->list_count;i++){
		if(i<list->list_count)
		node = node->next;
		node_adder(node);	
	}
	
	list->last = node;
	
	/*Printing the list in correct order*/
	printf("\nThe list of elements is:\n\n");
	for(i =1;i<=list->list_count;i++){
		printf("%s \t %s \t %s \t %lfx10^%d \n",node->element,node->n1,node->n2,node->value,node->power);
		node = node->prev;
	}
	
	node = list->head;

	/*Printing out the list of unique nodes*/
	printf("\n\nThe list of nodes are: \n");
	for(i=0;i<node_list_size;i++)
		printf("%s \n",node_list[i]);

	
	printf("\n\n");

	/*Initial conductance matrix to 0*/
	for(i=0;i<MAXVAR;i++)
		for(j=0;j<MAXVAR;j++);	
			cond_mat[i][j] = 0;

	cond_mat_size = node_list_size;
	
	
	/*Adding element stamps to the conduction matrix*/
	for(i=0;i<list->list_count;i++){
		element_stamp(node);
		node = node->next;
	}
	
	/*Finding ground node and setting it to 0*/
	int ng;	
	for(i=0;i<node_list_size;i++)
		if(strcmp(node_list[i],"GND")==0){
			ng = i;
			break;	
		}

	for(i=0;i<cond_mat_size;i++){
		if(ng==i)
			cond_mat[ng][ng]=1;
		else{
			cond_mat[i][ng]=0;
			cond_mat[ng][i]=0;
		}
	}
	i_vect[ng]=0;	

	/*Printing the conduction matrix rows from node equations*/
	printf("Conductance matrix size %d:\n\n",cond_mat_size);
	for(i=0;i<node_list_size;i++){
		for(j=0;j<cond_mat_size;j++){
			if(cimag(cond_mat[i][j])>0.0)		
				printf("%f + %fi \t",creal(cond_mat[i][j]),cimag(cond_mat[i][j]));
			else if(cimag(cond_mat[i][j])<0.0)
				printf("%f - %fi \t",creal(cond_mat[i][j]),-cimag(cond_mat[i][j]));
			else
				printf("%f \t\t",creal(cond_mat[i][j]));
		}
		printf("Node %s\n",node_list[i]);
	}

	/*Printing out the rows of the matrix coming from the extra V source equations*/
	for(i=node_list_size;i<(i_map_size+node_list_size);i++){

		for(j=0;j<cond_mat_size;j++){
			if(cimag(cond_mat[i][j])>0.0)		
				printf("%f + %fi \t",creal(cond_mat[i][j]),cimag(cond_mat[i][j]));
			else if(cimag(cond_mat[i][j])<0.0)
				printf("%f - %fi \t",creal(cond_mat[i][j]),-cimag(cond_mat[i][j]));
			else
				printf("%f \t\t",creal(cond_mat[i][j]));
		}
		printf("I in %s\n",i_map[i-node_list_size]);
	}

	printf("\n\nI Vector \n\n");
	/*Printing out the Current vector*/
	for(i=0;i<cond_mat_size;i++){
		if(cimag(i_vect[i])>0.0)		
				printf("%f + %fi \n",creal(i_vect[i]),cimag(i_vect[i]));
			else if(cimag(i_vect[i])<0.0)
				printf("%f - %fi \n",creal(i_vect[i]),-cimag(i_vect[i]));
			else
				printf("%f \n",creal(i_vect[i]));
	}
	
	list_free(list);;	

}

/*To insert the element stamp in the conductance matrix*/
void element_stamp(Node* node)
{	int n1,n2,n3,n4,i;
	
	for(i = 0;i<node_list_size;i++)
	{
		if(strcmp(node_list[i],(node->n1))==0)
			n1 = i;

		if(strcmp(node_list[i],(node->n2))==0)
			n2 = i;

		if(strcmp(node->n3,"\0"))
		{
			if(strcmp(node_list[i],(node->n3))==0)
				n3 = i;

			if(strcmp(node_list[i],(node->n4))==0)
				n4 = i;
		}
	}

	switch (node->element[0])
	{	
		case 'R':
			cond_mat[n1][n1] +=  1/(node->value); 
			cond_mat[n2][n2] +=  1/(node->value) ;
			cond_mat[n1][n2] +=  -1/(node->value);
			cond_mat[n2][n1] +=  -1/(node->value);
			break;

		case 'I':
			i_vect[n1] +=  -1*(node->value);
			i_vect[n2] +=  1*(node->value);
			break;

		case 'V':
			strcpy(i_map[i_map_size],(node->element));
			i_map_size++;
			cond_mat[n1][cond_mat_size] +=1;
			cond_mat[n2][cond_mat_size] +=-1;
			cond_mat[cond_mat_size][n1] += 1;
			cond_mat[cond_mat_size][n2] += -1;
			i_vect[cond_mat_size] += node->value; 
			cond_mat_size++;
			break;

		case 'C':
			cond_mat[n1][n1] +=  I*w*(node->value); 
			cond_mat[n2][n2] +=  I*w*(node->value) ;
			cond_mat[n1][n2] +=  -I*w*(node->value);
			cond_mat[n2][n1] +=  -I*w*(node->value);			
			break;

		case 'L':
			cond_mat[n1][n1] +=  -I/w/(node->value); 
			cond_mat[n2][n2] +=  -I/w/(node->value) ;
			cond_mat[n1][n2] +=  I/w/(node->value);
			cond_mat[n2][n1] +=  I/w/(node->value);			
			break;
		
	}	
	
}
