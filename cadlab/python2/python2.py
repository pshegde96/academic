#In this program we find the value of arctan(x) using the quad function,arctan() function, and by manually integrating 1/(1+x^2) using
#the trapezoid rule. We then analyse the errors and find out the value of h(width of trapezoid) for a corresponding
#value of tolerance.

#Importing libraries and defining some constants and variables.
from pylab import *
from scipy.integrate import quad
frequency = 51;
x_Max = 5.0;
x_Min = 0.0;
tolerance = 0.00000001;
#This stores the integral obtained from the quad function.
Integral1 = array([[0 for x in range(0,2)]for x in range(0,frequency)],dtype=float64);
#This array is used to store the values of h and their corresponding actual and estimated errors.
h = array([[0 for x in range(0,3)]for x in range(0,1000)],dtype = float64);
h_count = 0;

#Function that accepts x and returns 1/(1_x^2).
def funct(t):
	return (1/(1+t*t));

#This function take in a vector with elements at gap h, and returns elements at a gap h/2, with their 
#corresponding function(funct()) values.
def ftrap(x_old):
	global frequency;
	frequency = 2 * frequency - 1;
	x_new = linspace(x_Min,x_Max,frequency);
	f_new = funct(x_new);
	return ((x_new,f_new));

#This recursive function accepts  two vectors(x with elements at a gap of h, and their corresponding integral values).
#It then reduces the gap to h/2,calculates integral and calculates an estimated error using the previous integral values.
#If error is greater than tolerance, then it calls itself, else it returns.

def fintegral(x_old,y_old) :
	global frequency;global h;global h_count;
	(x_new,f_new) = ftrap(x_old);
	y_new = (x_Max - x_Min)/(frequency-1) * (cumsum(f_new)-0.5*(f_new + array(f_new[0] in range(0,len(x_new)))));
	h[h_count,0] = (x_Max - x_Min)/(frequency-1);
	error = abs(y_new[0:frequency:2] - y_old); #estimated error vector
	error = amax(error); #estimated error.
	h[h_count,1] = error; 
	act_error = amax(abs(y_new-arctan(x_new))); #Actual error.
	h[h_count,2] = act_error;

	if error > tolerance :
		h_count = h_count+1;
		fintegral(x_new,y_new);
	return;

#Form a vector x with gap of 0.1 and find the function vector and plot it.
x = linspace(x_Min,x_Max,frequency);
f = funct(x);

#Use quad function to find the integral of f which gives arctan(x).
for i in range(0,len(x)):
	Integral1[i] = (quad(funct,0,x[i]));

#Find the error between integral calculated using quad function and arctan(x).
atan = arctan(x);
error = Integral1[:,0] - atan;

#Calculate error using trapezoid rule using vector calculations only!
Integral2 = (x_Max - x_Min)/(frequency-1) * (cumsum(f)-0.5*(f + array(f[0] in range(0,len(x)))));

#This sets initial gap in x1 to 1 and finds the integral. And then fintegral() is called.
#The fintegral() executes recursively until it finds the max h for which error < tolerance.
#The estimated and actual errors are plotted against corresponding value of h.
frequency = 5;
x1 = linspace(x_Min,x_Max,frequency);
f1 = funct(x1);
Integral4 = (x_Max - x_Min)/(frequency-1) * (cumsum(f1)-0.5*(f1+ array(f1[0] in range(0,len(x1)))));
fintegral(x,Integral4);
act_error = h[1:h_count,2]
h = array(h,dtype = float64);


title(r"Plot of $1/(1+t^{2})$");
ylabel(r"f(x) = $1/(1+x^2)$");
xlabel(r'$x \rightarrow $');
plot(x,f,'r',label = 'f(x)');
legend(loc = 'upper right');
figure();

title(r"Plot of arctan(x)");
ylabel(r"f(x)");
xlabel(r'$x \rightarrow$');
plot(x,Integral1[:,0],'ro',label = 'quad fn');
plot(x,atan,label = '$tan^{-1}x$');
legend(loc = 'lower right');
figure();

title(r"Error in quad function");
ylabel(r"$Error$");
xlabel(r'$x \rightarrow $');
semilogy(x,error,'ro',label = 'Error in quad fn');
legend(loc = 'upper right');
figure();

title(r"Integral using trapezoid rule");
xlabel(r"$x \rightarrow $");
ylabel(r'$\int_{0}^{x}\frac{1}{1+t^{2}}dt$');
plot(x,Integral2,label = r'$\int_{0}^{x} \frac{1}{1+t^2}dt$');
legend(loc = 'lower right');
figure();

title(r'Plot of estimated and actual errors');
xlabel(r'$h \rightarrow$');
ylabel(r'$Error$');
loglog(h[1:h_count,0],h[1:h_count,1],'g+',label = 'Estimated error'); #h[:,1] has estimated error.
loglog(h[1:h_count,0],h[0:h_count-1,2],'ro',label = 'Actual error'); #h[:,2] has actual error.
legend(loc = 'lower right');

show();

