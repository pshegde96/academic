import re

filename = "spice.txt";
txt = open(filename);
lines = txt.readlines();
ele_name = []; #Stores the list of element names.
n1 = []; #Stores the list of 1st-nodes connected to the corresponding elements.
n2 = []; #Stores the list of 2nd-nodes connected to the corresponding elements.
value = []; #Stores the value of the corresponding element.
circuit_status = False; #Tells if the line is a part of the circuit description.

#Finding the section in between the .circuit and .end and printing it.
print '\n\nPrinting the elements that describe the circuit:\n';
for line in lines:
	if ".end" in line:
		circuit_status = False;
		break;

	if circuit_status:
		print line;

	if ".circuit" in line:
		circuit_status = True;

#Parsing the circuit description line into 4 parts:element name,node1,node2,value.
for line in lines:
	if ".end" in line:
		circuit_status = False;
		break;
	if circuit_status:
		words = line.split();
		ele_name.append(words[0]);
		n1.append(words[1]);
		n2.append(words[2]);
		value.append(words[3]);

	if ".circuit" in line:
		circuit_status = True;

#Defining the modifiers and the value they correspond to.
mods={'p':1.e-12,'n':1e-9,'u':1e-6,'m':1e-3,'':1,'k':1e3,'meg':1e6};

#Parsing the value[] string.
for i in range(0,len(value)):
	#Split the value into 3 parts:the number,the exponential,the modifier.
	k=re.search("^([+-]?[0-9]*[\.]?[0-9]*)(e[+-]?[0-9]+)?(.*)",value[i]);
	#To incorporate the exponential part of the value string:
	#if re.search() returns a None type, multiply by 1,else multiply the value it carries.	
	if k.groups()[1] is None:
		power = 1;
	else:
		power = float(k.groups()[1]);
	#Assigning the value[](float type) its value from value[](string type)'s content.
	value[i] = (float(k.groups()[0]))*mods[k.groups()[2]]*power;

#Printing out the circuit elements once the value has been parsed.
print '\n\nPrinting the Reconstructed list of elements:\n';
for i in range(0,len(ele_name)):
	print ('%s %s %s %s'%(ele_name[i],n1[i],n2[i],value[i]));

#Forming a node dictionary
nodes = dict();
node_index = 1; #Indicates the index of the new node that is being added to the dictionary.
for i in range(0,len(ele_name)):
	#If n1[i] is not already on the list, then add it		
	if n1[i] not in nodes:
		#If it is ground, then its index is 0
		if n1[i] in ("GND","0"):
			nodes[n1[i]]=0;
		else:
			nodes[n1[i]]=node_index;
			node_index+=1;

	if n2[i] not in nodes:
		if n2[i] in ("GND","0"):
			nodes[n2[i]]=0;
		else:
			nodes[n2[i]]=node_index;
			node_index+=1;

#Printing the nodes in the order of the node-index assigned to them.
print '\n\nPrinting the nodes in the order of the node-index assigned to them:\n';
for i in range(0,len(nodes)):
	for n in nodes:
		if nodes[n] is i:
			print ('%d: %s')%(i,n);
			break;
print '\n';


