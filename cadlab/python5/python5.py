from pylab import *
import mpl_toolkits.mplot3d.axes3d as p3
from scipy.linalg import lstsq
import sys

if(len(sys.argv)==6):
	Nx = int(sys.argv[1]);
	Ny = int(sys.argv[2]);
	Nbegin = int(sys.argv[3]);
	Nend = int(sys.argv[4]);
	Niter = int(sys.argv[5]);

else:
	Nx = 25;
	Ny = 25;
	Nbegin = 8;
	Nend = 17;
	Niter = 1500;

phi = array([[0 for i in range(0,Ny)]for j in range(0,Nx)],dtype = float64);
phi[0,Nbegin-1:Nend] = 1;
error = array([0 for i in range(0,Niter)],dtype = float64);

#Solving for phi using the Laplace Equation.
for k in range(0,Niter) :
	oldphi = phi.copy(); #Save a copy of old phi to calculate relative error.
	#Finding the average for interior elements
	phi[1:-1,1:-1] = 0.25*(phi[0:-2,1:-1] + phi[2:,1:-1] + phi[1:-1,0:-2] + phi[1:-1,2:]);
	phi[1:-1,0] = phi[1:-1,1];	#Boundary condtn for 1st column.
	phi[1:-1,Ny-1] = phi[1:-1,Ny-2];	#Boundary condtn for last column.
	phi[0,1:-1] = phi[1,1:-1];	#Boundary condtn for 1st row.
	phi[Nx-1,1:-1] = phi[Nx-2,1:-1];	#Boundary condtn for last row.
	phi[0,Nbegin-1:Nend] = 1;	#Overwriting for + electrode.
	phi[Ny-1,Nbegin-1:Nend] = 0;	#Overwriting for ground electrode.
	error[k] = (abs(phi-oldphi)).max(); 

#Forming the matrix for: logy = logA + B.x
xk = arange(0,Niter);
M0 = array([1 for i in range(0,Niter)],dtype = float32);
M1 = xk;
M = c_[M0,M1];
#Taking values from the start will lead to an erroneous fit
err_inaccurate,resid,rank,sig = lstsq(M,log(error));
#Take values after 600 for a better plot.
err,resid1,rank1,sig1 = lstsq(M[600:],log(error[600:]));
A = exp(err[0]);
B = err[1];

#Converting the log equations back to the original equations: y = A*exp(B.x)
fit_inaccurate = exp(err_inaccurate[0])*exp(err_inaccurate[1]*xk);
fit = exp(err[0])*exp(err[1]*xk[600:]);

#Absolute error
abs_err = A*exp(Niter*B)/(1-exp(B)); #Sum of errors from Niter to infinity.
abs_err_per = abs_err*100;
print "Absoulte error = %f"%abs_err_per;

#Error plot.
title('Error in Potential against number of Iterations:');
xlabel(r"Number of iterations $\rightarrow$");
ylabel(r"Error in Potential $\rightarrow$");
semilogy(xk[::50],error[::50],'ro',label = 'Error');
semilogy(xk,fit_inaccurate,'b',label = 'Inaccurate fit');
semilogy(xk[600:],fit,'r',label = 'Accurate fit');
legend(loc = 'upper right');	
show();

#Surface plot of the Potential.
x = arange(1,Nx+1);
y = arange(1,Ny+1);
X,Y = meshgrid(x,y);
fig = figure()
ax = fig.gca(projection='3d');
ax.set_title('Surface plot of potential in the resistor:');
ax.set_xlabel(r"x$\rightarrow$");
ax.set_ylabel(r"y$\rightarrow$");
ax.set_zlabel(r"Potential$\rightarrow$");
surf = ax.plot_surface(Y, X, phi, rstride=1, cstride=1, cmap=cm.jet,linewidth=0, antialiased=False);
show();

#Contour plot
title('Contour Plot of Potential');
xlabel(r"x$\rightarrow$")
ylabel(r"y$\rightarrow$")
contour(X,Y,phi);
show();

#Quiver plot
Jx = zeros((Ny,Nx),dtype=float32);
Jy = zeros((Nx,Ny),dtype=float32);
Jx[:,1:-1] = 0.5*(phi[:,0:-2]-phi[:,2:]);
Jy[1:-1,:] = 0.5*(phi[0:-2,:]-phi[2:,:]);
title('Vector Plot of current:');
quiver(Y,X,Jy,Jx);
show();
