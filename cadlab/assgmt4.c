#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>
#define MAXLENGTH 512

/*Global Variables*/

int circuit_status = 0; /*Tells whether the current line being read is part of the circuit description*/

/*Defining the structure of the double linked list*/
typedef struct Node{
	struct Node* prev;
	struct Node* next;
	char* element;
	char *n1,*n2,*n3,*n4;
	int value,power;
}Node;

typedef struct DList{
	Node* head;
	Node* last;
	int list_count;
}DList;

/*The linereader function */
int linereader(char* buff,Node* node)
{
	if(circuit_status ==1){
		char str1[5],str2[4],str3[4],str4[4],str5[4],str6[4];
		int word_count;
		word_count = sscanf(buff,"%s %s %s %s %s %s",str1,str2,str3,str4,str5,str6);
		
		if(word_count==6){
			strcpy(node->element,str1);
			strcpy(node->n1,str2);
			strcpy(node->n2,str3);
			strcpy(node->n3,str4);
			strcpy(node->n4,str5);
			node->value = atoi(str6);
			//node->power = 	
		}

		if(word_count==4){
			strcpy(node->element,str1);
			strcpy(node->n1,str2);
			strcpy(node->n2,str3);
			node->value = atoi(str4);
			//node->power
		}
	//printf("%s %s \n",node->element,node->n1);
	}
	
	
	
	if(buff[0]=='.'){
		char checker[10];
		sscanf(buff,".%s",checker);
		if(!strcmp(checker,"circuit")){
			circuit_status = 1;
			return 0;
		}
		else if(!strcmp(checker,"end")){
			circuit_status = 0;	
			
		}	
	}
	return 1;

}

/*add() function adds a new node to the list*/
void add(Node* new,Node* node){
	
	
	node->next = new;
	new->prev = node;
	new->next = NULL;
	node = new;
}

/*Function to output list in correct order*/
void output_correct_order(Node* node,int i){

	printf("Element %d: \t %s  %s  %s",i,node->element,node->n1,node->n2);
	if(node->n3!='\0')
		printf("  %s  %s",node->n3,node->n4);
	printf("  %d \n",node->value);
	
	node = node->next;	
}

void output_reverse_order(Node* node,int i){

	printf("Element %d: \t %s  %s  %s",i,node->element,node->n1,node->n2);
	if(node->n3!='\0')
		printf("  %s  %s",node->n3,node->n4);
	printf("  %d \n",node->value);
	
	node = node->prev;	
}

/*The MAIN function*/
int main(int argc,char** argv)
{
	
	/*Initiallising the the double linked list*/	
	
	DList* list = (DList*) malloc(sizeof(DList*));
	assert(list != NULL);
	list->head = NULL;
	list->last = NULL;
	list->list_count = 1;
	/*End of initiallisation of dll*/

	/*Reading in the spice netlist file*/
	FILE *fp = fopen(argv[1],"r");
	if(fp==NULL){
		printf("Could not open the file,exiting");
		exit(1);
	}

	char buff[MAXLENGTH];

	while(fgets(buff,MAXLENGTH,fp))
	{	/*linereader reads and makes sense of the line*/
		/*If it returns 1, then an element has been read and a node*/
		/*has to be added*/
		linereader(buff,list->head);
		if(circuit_status==1){
			Node* new = (Node*) malloc(sizeof(Node*));
			assert(new!=NULL);	
			new->next = NULL;
			new->prev = NULL;
			add(new,list->head);
			list->list_count++;
			
			}
			
	}
	
	int i;
	Node* node = list->head;
	
	/*Printing the list in correct order*/
	printf("\nThe list of elements in correct order is: \n\n");
	
	for(i=1;i<=list->list_count;i++){
		//output_correct_order(node1,i);
		printf("Element %d: \t %s  %s  %s",i,node->element,node->n1,node->n2);
		if(node->n3!='\0')
			printf("  %s  %s",node->n3,node->n4);
		printf("  %d \n",node->value);
	
		node = node->prev;	
	}

	/*Printing the list in reverse order*/
	printf("\nThe list of elements in reverse order is:\n\n");
	node = list->last;
	for(i =1;i<=list->list_count;i++){
		//output_reverse_order(node1,i);
	}
}
