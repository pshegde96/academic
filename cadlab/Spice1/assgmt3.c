
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define MAXLENGTH 512
#define MAXNODES 20
/*Global Variables*/

int circuit_status = 0; /*Tells whether the current line being read is part of the circuit description*/
char node_list[MAXNODES][10];/*Saves the list of unique nodes*/
int node_list_size=0;/*Saves the number of nodes*/
int node_map[MAXNODES][MAXNODES];/*To map the connection between nodes*/
/*Defining the structure of the double linked list*/
typedef struct Node{
        struct Node* prev;
        struct Node* next;
        char element[10];
        char n1[10];
        char n2[10];
        char n3[10];
        char n4[10];
        float value;
        int power;
}Node;

typedef struct DList{
        Node* head;
        Node* last;
        int list_count;
}DList;



/*add() function adds a new node to the list*/
void add(Node* new,DList* list)
{
        if(list->list_count==0){
                list->head = new;
                list->list_count++;     
        }
        else {
                new->next = list->head;
                list->head->prev = new;
                
                list->head=new;
                list->list_count++;
        }
}
/*Function to find unique nodes and add it to node_list*/
void node_adder(Node* node)
{
        int i,flag1 = 0,flag2 = 0;
        for(i=0;i<node_list_size;i++){
                if(strcmp(node->n1,node_list[i])==0)
                        flag1 =1;
                if(strcmp(node->n2,node_list[i])==0)
                        flag2 =1;
        }
        if(flag1 == 0){
                strcpy(node_list[node_list_size],node->n1);     
                node_list_size +=1;
        }
        if(flag2==0){
                strcpy(node_list[node_list_size],node->n2);
                node_list_size += 1;
        }
        
        /*When element has 4 nodes*/
        if(strcmp("\0",node->n3)!=0){
                int flag3=0,flag4=0;
                
                        
                for(i=0;i<node_list_size;i++){
                        if(strcmp(node->n3,node_list[i])==0)
                                flag3 =1;
                        if(strcmp(node->n4,node_list[i])==0)
                                flag4 =1;
                }
                if(flag3 == 0){
                        strcpy(node_list[node_list_size],node->n3);     
                        node_list_size +=1;
                }
                if(flag4==0){
                        strcpy(node_list[node_list_size],node->n4);
                        node_list_size += 1;
                }
        }       
                
}

/*Function to map the connection between nodes*/
void node_mapper(Node* node)
{       
        int i=0;
        int N1,N2;
        while(i<node_list_size){
                if(strcmp(node_list[i],node->n1)==0)
                        N1 = i;
                                
                if(strcmp(node_list[i],node->n2)==0)
                        N2 = i;
                i=i+1;          
        }
        node_map[N1][N2]+=1;
        node_map[N2][N1]+=1;

        /*For elements with 4 nodes*/
        if(strcmp("\0",node->n3)!=0){
                int N3,N4;
                i=0;
                while(i<node_list_size){
                        if(strcmp(node_list[i],node->n3)==0)
                                N3 = i;
                                
                        if(strcmp(node_list[i],node->n4)==0)
                                N4 = i;
                        i=i+1;          
                }
                node_map[N3][N4]+=1;
                node_map[N4][N3]+=1;            
        }       
}



/*The MAIN function*/
int main(int argc,char** argv)
{
        
        /*Initiallising the the double linked list*/    
        
        DList* list = (DList*) malloc(sizeof(DList));
        
        assert(list != NULL);
        list->head = NULL;
        
        list->list_count = 0;
        /*End of initiallisation of dll*/

        /*Reading in the spice netlist file*/
        FILE *fp = fopen(argv[1],"r");
        if(fp==NULL){
                printf("Could not open the file,exiting");
                exit(1);
        }

        char buff[MAXLENGTH];
        /*Reading the file line by line*/
        while(fgets(buff,MAXLENGTH,fp))
        {       
                /*Checking whether the line is a part of ckt description*/
                char checker[10];
                sscanf(buff,"%s",checker);
                
                
                
                if(strcmp(checker,".end")==0){
                        circuit_status = 0;     
                        
                        }
                if(strcmp(checker,".circuit")==0){
                        circuit_status = 1;
                                
                        }       
                /*Ignore the line if not part of ckt description*/
                if(circuit_status==0)
                        continue;
                if((checker[0]=='.')||(checker[0]=='\0'))
                        continue;

                /*Node to save the details of the element and add it to list*/
                Node* new = (Node*) malloc(sizeof(Node));
                assert(new!=NULL);      
                
                /*set pow as x for for 0 power*/
                char str1[10],str2[10],str3[10],str4[10],str5[10],str6[10],pow[4]="x";
                
                /*Check 1st character for element type and proceed*/
                switch (buff[0]){

                        case 'R':
                        case 'L':
                        case 'C':
                        case 'V':
                        case 'I':
                                sscanf(buff,"%s %s %s %s",str1,str2,str3,str4);
                                
                                strcpy(new->element,str1);
                                strcpy(new->n1,str2);
                                strcpy(new->n2,str3);
                                strcpy(new->n3,"\0"); /*To say it has only 2 nodes*/
                                sscanf(str4,"%f%s",&(new->value),pow);
                                new->next = NULL;
                                new->prev = NULL;
                                 
                                break;  
                        
                        case 'E':
                        case 'G':
                        case 'H':
                        case 'F':
                                sscanf(buff,"%s %s %s %s %s %s",str1,str2,str3,str4,str5,str6);
                                strcpy(new->n1,str2);
                                strcpy(new->element,str1);
                                
                                strcpy(new->n2,str3);
                                strcpy(new->n3,str4);
                                strcpy(new->n4,str5);
                                sscanf(str6,"%f%s",&(new->value),pow);
                                
                                break;
                        default :
                                printf("Invalid element\n");
                                continue;
                        }
                        /*Assigning proper value for power*/
                        switch (pow[0]){
                                case 'u': new->power = -6;break;
                                case 'm': new->power = -3;break;
                                case 'n': new->power = -9;break;
                                case 'k': new->power = 3;break;
                                case 'p': new->power = -12;break;
                                case 'x': new->power = 0;break;
                                default: printf("Invalid Power for %s\n",str1);
                        }               
                        if(strcmp(pow,"meg")==0)
                                new->power = 6;
                        
                        /*Add the newly described element to the list*/
                        add(new,list);
        }
        
        
        int i,j;        /*Loop counters*/
        
        /*Printing the list in reverse order*/
        printf("\nThe list of elements in reverse order is: \n\n");
        Node *node = list->head;
        for(i=1;i<=list->list_count;i++){
                printf("%s \t %s \t %s \t %fx10^%d \n",node->element,node->n1,node->n2,node->value,node->power);
                
                if(i<list->list_count)
                node = node->next;
                node_adder(node);       
        }
        
        list->last = node;
        
        /*Printing the list in correct order*/
        printf("\nThe list of elements in correct order is:\n\n");
        for(i =1;i<=list->list_count;i++){
                printf("%s \t %s \t %s \t %fx10^%d \n",node->element,node->n1,node->n2,node->value,node->power);
                node = node->prev;
        }
        
        node = list->head;

        /*Printing out the list of unique nodes*/
        printf("\n\nThe list of nodes are: \n");
        for(i=0;i<node_list_size;i++)
                printf("%s \n",node_list[i]);

        /*Setting all initial connections to 0*/
        for(i=0;i<MAXNODES;i++)
                for(j=0;j<MAXNODES;j++)
                        node_map[i][j]=0;

        /*Mapping the different node connections*/
        for(i=0;i<list->list_count;i++)
        {
                node_mapper(node);
                node = node->next;
        }

        /*Printing out the connection between nodes*/
        printf("\n\nThe connection between the nodes is:\n");
        for(i=0;i<node_list_size;i++){
                printf("%s connected to: ",node_list[i]);
                for(j=0;j<node_list_size;j++)
                        if(node_map[i][j]>0)
                                printf("%s,",node_list[j]);
                printf("\n");
        }
        printf("\n\n");
}
